CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * API
 * Demo
 * Maintainers
 * Images

Introduction
------------
In the era of smart phone, generating leads via website should be easy. It
should be easy for your visitors to connect you quickly by her most preferred
apps. Say SMS or skype or Facetime or Facebook messenger. So this module try to
make the better engagement with visitors. 

Requirements
------------
Needs drupal block module which is part of core. Client side 

Installation
------------
Install your module like any other drupal 7 module. More info here
https://www.drupal.org/documentation/install/modules-themes/modules-7

Configuration
-------------
go to admin/config/services/reachus and enter your service ID and save. 
You can select the icon size, text or image display. more document here
http://http://drupal.journalpiece.com/node/2

You can give permission to other roles in permission page under the title
"Reach us Settings"

Detail configuration tutorial here here : http://drupal.journalpiece.com/node/2

API
---
See the API and development document here http://drupal.journalpiece.com/node/2

Demo 
----
http://drupal.journalpiece.com/node/1

Maintainers
-----------
https://www.drupal.org/u/quardz

Images
------
Part of the images are designed myself and PSD and AI files are available to
download, more info here http://drupal.journalpiece.com/node/3 and rest of the
files are taken from GPL licensed image from here http://genericons.com
